#!/usr/bin/env python
# encoding: utf-8
"""
Adafruit_L3GD20.py

Created by Shigeru KAWAGUCHI on 2013-01-20.
Copyright (c) 2013 __Lamb Informatics Limited__. All rights reserved.
"""

import sys
import os
import smbus
from Adafruit_I2C import Adafruit_I2C
import unittest


class L3GD20:
	i2c				= None
	dataRate		= 0b00
	bandWidth		= 0b00
	powerDown		= 0b0
	axesEnable		= 0b111
	hpm				= 0b00
	hpcf			= 0b0000
	int1Enable		= 0b0
	int1Boot		= 0b0
	int1Active		= 0b0
	ppOd			= 0b0
	int2Drdy		= 0b0
	int2Wtm			= 0b0
	int2Orun		= 0b0
	int2Empty		= 0b0
	bdu				= 0b0
	ble				= 0b0
	fullScale		= 0b00
	sim				= 0b0
	boot			= 0b0
	fifoEnable		= 0b0
	hpEnable		= 0b0
	int1Select		= 0b00
	outSelect		= 0b00
	refVal			= 0b00000000
	fifoMode		= 0b000
	fifoWtm			= 0b00000
	int1AndOr		= 0b0
	int1LIR			= 0b0
	int1Zhie		= 0b0
	int1Zlie		= 0b0
	int1Yhie		= 0b0
	int1Ylie		= 0b0
	int1Xhie		= 0b0
	int1Xlie		= 0b0
	int1ThsXH		= 0b0000000
	int1ThsXL		= 0b00000000
	int1ThsYH		= 0b0000000
	int1ThsYL		= 0b00000000
	int1ThsZH		= 0b0000000
	int1ThsZL		= 0b00000000
	int1Wait		= 0b0
	int1Duration	= 0b0000000
	convFactor		= 0.00875
	
	# Device Address
	__L3GD20_ADDRESS	= 0xD4
	__L3GD20_ID			= 0b11010100
	
	# L3GD20 Registers	addr		type	default
	__WHO_AM_I			= 0x0F	#	r		11010100
	__CTRL_REG1			= 0x20	#	rw		00000111
	__CTRL_REG2			= 0x21	#	rw		00000000
	__CTRL_REG3			= 0x22	#	rw		00000000
	__CTRL_REG4			= 0x23	#	rw		00000000
	__CTRL_REG5			= 0x24	#	rw		00000000
	__REFERENCE			= 0x25	#	rw		00000000
	__OUT_TEMP			= 0x26	#	r
	__STATUS_REG		= 0x27	#	r
	__OUT_X_L			= 0x28	#	r
	__OUT_X_H			= 0x29	#	r
	__OUT_Y_L			= 0x2A	#	r
	__OUT_Y_H			= 0x2B	#	r
	__OUT_Z_L			= 0x2C	#	r
	__OUT_Z_H			= 0x2D	#	r
	__FIFO_CTRL_REG		= 0x2E	#	rw		00000000
	__FIFO_SRC_REG		= 0x2F	#	r
	__INT1_CFG			= 0x30	#	rw		00000000
	__INT1_SRC			= 0x31	#	r
	__INT1_TSH_XH		= 0x32	#	rw		00000000
	__INT1_TSH_XL		= 0x33	#	rw		00000000
	__INT1_TSH_YH		= 0x34	#	rw		00000000
	__INT1_TSH_YL		= 0x35	#	rw		00000000
	__INT1_TSH_ZH		= 0x36	#	rw		00000000
	__INT1_TSH_ZL		= 0x37	#	rw		00000000
	__INT1_DURATION		= 0x38	#	rw		00000000

	def getPiRevision(self):
		"Gets the version number of the Raspberry Pi board"
		# Courtesy quick2wire-python-api
		# https://github.com/quick2wire/quick2wire-python-api
		try:
			with open('/proc/cpuinfo','r') as f:
				for line in f:
					if line.startswith('Revision'):
						return 1 if line.rstrip()[-1] in ['1','2'] else 2
		except:
			return 0
		pass

	def __twos_comp(self, val, bits):
		"compute the 2's compliment of int value val"
		if( (val&(1<<(bits-1))) != 0 ):
			val = val - (1<<bits)
		return val
		pass

	# Constructor
	def __init__(self, address=0x6b, debug=False):
		self.i2c = Adafruit_I2C(address, smbus.SMBus(1 if self.getPiRevision() > 1 else 0), debug)
		self.address = address
		self.debug = debug
		# Check if connected device is L3GD20
		if self.i2c.readU8(self.__WHO_AM_I) != self.__L3GD20_ID:
			raise DeviceError('Not connected to L3GD20.')
		# Initialize registers
		self.i2c.write8(self.__CTRL_REG1, 0x0F)
		pass

	def readData(self):
		"Read a set of raw data from Gyro"
		xlo = self.i2c.readU8(self.__OUT_X_L)
		if (self.debug):
			print "DBG: gyro X lo: 0x%04X (%d)" % (xlo & 0xFFFF, xlo)
		xhi = self.i2c.readU8(self.__OUT_X_H)
		if (self.debug):
			print "DBG: gyro X hi: 0x%04X (%d)" % (xhi & 0xFFFF, xhi)
		ylo = self.i2c.readU8(self.__OUT_Y_L)
		if (self.debug):
			print "DBG: gyro Y lo: 0x%04X (%d)" % (ylo & 0xFFFF, ylo)
		yhi = self.i2c.readU8(self.__OUT_Y_H)
		if (self.debug):
			print "DBG: gyro Y hi: 0x%04X (%d)" % (yhi & 0xFFFF, yhi)
		zlo = self.i2c.readU8(self.__OUT_Z_L)
		if (self.debug):
			print "DBG: gyro Z lo: 0x%04X (%d)" % (zlo & 0xFFFF, zlo)
		zhi = self.i2c.readU8(self.__OUT_Z_H)
		if (self.debug):
			print "DBG: gyro Z hi: 0x%04X (%d)" % (zhi & 0xFFFF, zhi)
		# construct returning 3D object
		gyroData = Obj3D()
		gyroData.x = ((xhi << 8) + xlo)
		gyroData.y = ((yhi << 8) + ylo)
		gyroData.z = ((zhi << 8) + zlo)
		return gyroData
		pass

	def readDps(self):
		"Read a set of data from Gyro and convert into degree per second and return the value"
		data = self.readData()
		# convert each into degrees per second
		gyroValue = Obj3D()
		gyroValue.x = self.__twos_comp(data.x, 16) * self.convFactor
		gyroValue.y = self.__twos_comp(data.y, 16) * self.convFactor
		gyroValue.z = self.__twos_comp(data.z, 16) * self.convFactor
		return gyroValue
		pass

	def setOutputDataRate(self, val = 95):
		if val == 95:
			self.dataRate = 0b00
		elif val == 190:
			self.dataRate = 0b01
		elif val == 380:
			self.dataRate = 0b10
		elif val == 760:
			self.dataRate = 0b11
		else:
			print "Accepted values are 95, 190, 380 and 760"
		self.__setCtrlReg1()
		pass

	def setBandwidth(self, val):
		"sets self.bandWidth value"
		pass

	def setPower(self, val = True):
		if val:
			self.powerDown = 0b1
		else:
			self.powerDown = 0b0
		self.__setCtrlReg1()
		pass

	def setAxesEnable(self, x = True, y = True, z = True):
		if x:
			xBit = 0b1
		else:
			xBit = 0b0
		if y:
			yBit = 0b1
		else:
			yBit = 0b0
		if z:
			zBit = 0b1
		else:
			zBit = 0b0
		self.axesEnable = (zBit << 2) + (yBit << 1) + xBit
		self.__setCtrlReg1()
		pass

	def __setCtrlReg1(self):
		param = (self.dataRate << 6) + (self.bandWidth << 4) + (self.powerDown << 3) + self.axesEnable
		if (self.debug):
			print "DBG: CTRL_REG1(20h): 0x%02X (%d)" & (param & 0xFF, param)
		#self.i2c.write8(self.__CTRL_REG1, param)
		pass

	def setHiPassFilterMode(self, val = "normal HP_RESET_FILTER"):
		print "To be implemented"
		pass

	def setHiPassFilterCutOff(self, val):
		pass

	def __setCtrlReg2(self):
		param = (0b00 << 6) + (self.hpm << 4) + self.hpcf
		if (self.debug):
			print "DBG: CTRL_REG2(21h): 0x%02X (%d)" & (param & 0xFF, param)
		#self.i2c.write8(self.__CTRL_REG2, param)
		pass

	def setInterrupt1Enable(self, val = False):
		if val:
			self.int1Enable = 0b1
		else:
			self.int1Enable = 0b0
		self.__setCtrlReg3()
		pass

	def __setCtrlReg3(self):
		param = (self.int1Enable << 7) + (self.int1Boot << 6) + (self.int1Active << 5) + (self.ppOd << 4) + (self.int2Drdy << 3) + (self.int2Wtm << 2) + (self.int2Orun << 1) + self.int2Empty
		if (self.debug):
			print "DBG CTRL_REG3(22h): 0x%02X (%d)" & (param & 0xFF, param)
		#self.i2c.write8(self.__CTRL_REG3, param)
		pass

	def setBlockDataUpdate(self, val = False):
		if val:
			self.bdu = 0b1
		else:
			self.bdu = 0b0
		self.__setCtrlReg4()
		pass

	def setBigEndian(self, val = False): 
		if val:
			self.ble = 0b1
		else:
			self.ble = 0b0
		self.__setCtrlReg4()
		pass

	def setFullScale(self, val = 250):
		if val == 250:
			self.fullScale = 0b00
			self.convFactor = 0.00875
		elif val == 500:
			self.fullScale = 0b01
			self.convFactor = 0.01750
		elif val == 2000:
			self.fullScale = 0b10
			self.convFactor = 0.070
		else:
			print "Accepted values are 250, 500 and 2000 (default 250)"
		self.__setCtrlReg4()
		pass

	def setSpiMode(self, val = 4):
		if val == 4:
			self.sim = 0b0
		elif val == 3:
			self.sim = 0b1
		else:
			print "Accepted values are 3 and 4 (default 4)"
		self.__setCtrlReg4()
		pass

	def __setCtrlReg4(self):
		param = (self.bdu << 7) + (self.ble << 6) + (self.fullScale << 4) + (0b000 << 1) + self.sim
		if (self.debug):
			print "DBG CTRL_REG4(23h): 0x%02X (%d)" & (param & 0xFF, param)
		#self.i2c.write8(self.__CTRL_REG4, param)
		pass

	def __setCtrlReg5(self):
		param = (self.boot << 7) + (self.fifoEnable << 6) + (self.hpEnable << 4) + (self.int1Select << 2) + self.outSelect
		if (self.debug):
			print "DBG CTRL_REG5(24h): 0x%02X (%d)" & (param & 0xFF, param)
		#self.i2c.write8(self.__CTRL_REG5, param)
		pass
	
	def __setReference(self):
		param = self.refVal
		if (self.debug):
			print "DBG REFERENCE(25h) 0x%02X (%d)" & (param & 0xFF, param)
		#self.i2c.write8(self.__REFERENCE, param)
		pass
	
	def readStatus(self):
		"Read STATUS_REG(27h) raw data"
		status = self.i2c.readU8(self.__STATUS_REG)
		if (self.debug):
			print "DBG: STATUS_REG(27h): 0x%02X (%d)" % (status & 0xFF, status)
		return status
		pass

	def readDataOverrun(self):
		"Return boolean for data overun"
		if self.readStatus() & 0b10000000 != 0:
			return True
		else:
			return False
		pass

	def readXOverrun(self):
		"Return boolean for X axis data overun"
		if self.readStatus() & 0b01000000 != 0:
			return True
		else:
			return False
		pass

	def readYOverrun(self):
		"Return boolean for Y axis data overun"
		if self.readStatus() & 0b00100000 != 0:
			return True
		else:
			return False
		pass

	def readZOverrun(self):
		"Return boolean for Z axis data overun"
		if self.readStatus() & 0b00010000 != 0:
			return True
		else:
			return False
		pass

	def dataAvailable(self):
		"Return boolean for data available"
		if self.readStatus() & 0b00001000 != 0:
			return True
		else:
			return False
		pass

	def readXAvailable(self):
		"Return boolean for X data available"
		if self.readStatus() & 0b00000100 != 0:
			return True
		else:
			return False
		pass

	def readYAvailable(self):
		"Return boolean for Y data available"
		if self.readStatus() & 0b00000010 != 0:
			return True
		else:
			return False
		pass

	def readZAvailable(self):
		"Return boolean for Z data available"
		if self.readStatus() & 0b00000001 != 0:
			return True
		else:
			return False
		pass

	def __setFifoCtrlReg(self):
		"sets FIFO_CTRL_REG(2Eh)"
		param = (self.fifoMode << 5) + self.fifoWtm
		if (self.debug):
			print "DBG FIFO_CTRL_REG(2Eh) 0x%02X (%d)" & (param & 0xFF, param)
		#self.i2c.write8(self.__FIFO_CTRL_REG, param)
		pass

	def readFifoSrc(self):
		"Read FIFO_SRC_REG(2Fh) data"
		fifoSrc = self.i2c.readU8(self.__FIFO_SRC_REG)
		if (self.debug):
			print "DBG: FIFO_SRC_REG(2Fh): 0x%02X (%d)" % (fifoSrc & 0xFF, fifoSrc)
		return fifoSrc
		pass

	def readFifoWatermark(self):
		"Return boolean for FIFO watermark"
		if self.readFifoSrc() & 0b10000000 != 0:
			return True
		else:
			return False
		pass

	def readFifoOverrun(self):
		"Return boolean for FIFO overrun"
		if self.readFifoSrc() & 0b01000000 != 0:
			return True
		else:
			return False
		pass

	def readFifoEmpty(self):
		"Return boolean for FIFO empty"
		if self.readFifoSrc() & 0b00100000 != 0:
			return True
		else:
			return False
		pass

	def readFifoLevel(self):
		"Return FIFO stored data level"
		data = (self.readFifoSrc() & 0b00011111)
		return data #self.__twos_comp(data, 5)

	def __setInt1Cfg(self):
		"sets INT1_CFG(30h)"
		param = (self.int1AndOr << 7) + (self.int1LIR << 6) + (self.int1Zhie << 5) + (self.int1Zlie << 4) + (self.int1Yhie << 3) + (self.int1Ylie << 2) + (self.int1Xhie << 1) + self.int1Xlie
		if (self.debug):
			print "DBG INT1_CFG(30h) 0x%02X (%d)" & (param & 0xFF, param)
		#self.i2c.write8(self.__INT1_CFG, param)
		pass

	def readInt1Src(self):
		"Read INT1_SRC(31h)"
		int1Src = self.i2c.readU8(self.__INT1_SRC)
		if (self.debug):
			print "DBG: INT1_SRC(31h): 0x%02X (%d)" % (int1Src & 0xFF, int1Src)
		return int1Src
		pass

	def readInt1Active(self):
		"Return boolean for Interrupt 1 Active"
		if self.readInt1Src() & 0b01000000 != 0:
			return True
		else:
			return False
		pass

	def readInt1Zhi(self):
		"Return boolean for Z high event"
		if self.readInt1Src() & 0b00100000 != 0:
			return True
		else:
			return False
		pass

	def readInt1Zlo(self):
		"Return boolean for Z low event"
		if self.readInt1Src() & 0b00010000 != 0:
			return True
		else:
			return False
		pass

	def readInt1Yhi(self):
		"Return boolean for Y high event"
		if self.readInt1Src() & 0b00010000 != 0:
			return True
		else:
			return False
		pass

	def readInt1Ylo(self):
		"Return boolean for Y low event"
		if self.readInt1Src() & 0b00000100 != 0:
			return True
		else:
			return False
		pass

	def readInt1Xhi(self):
		"Return boolean for X high event"
		if self.readInt1Src() & 0b00000010 != 0:
			return True
		else:
			return False
		pass

	def readInt1Xlo(self):
		"Return boolean for X low event"
		if self.readInt1Src() & 0b00000001 != 0:
			return True
		else:
			return False
		pass

	def __setInt1ThsXH(self):
		"sets INT1_THS_XH(32h)"
		if (self.debug):
			print "DBG INT1_THS_XH(32h) 0x%02X (%d)" & (self.int1ThsXH & 0xFF, self.int1ThsXH)
		#self.i2c.write8(self.__INT1_TSH_XH, param)
		pass

	def __setInt1ThsXL(self):
		"sets INT1_THS_XL(33h)"
		if (self.debug):
			print "DBG INT1_THS_XL(33h) 0x%02X (%d)" & (self.int1ThsXL & 0xFF, self.int1ThsXL)
		#self.i2c.write8(self.__INT1_TSH_XL, param)
		pass

	def __setInt1ThsYH(self):
		"sets INT1_THS_YH(34h)"
		if (self.debug):
			print "DBG INT1_THS_YH(34h) 0x%02X (%d)" & (self.int1ThsYH & 0xFF, self.int1ThsYH)
		#self.i2c.write8(self.__INT1_TSH_YH, param)
		pass

	def __setInt1ThsYL(self):
		"sets INT1_THS_YL(35h)"
		if (self.debug):
			print "DBG INT1_THS_YL(35h) 0x%02X (%d)" & (self.int1ThsYL & 0xFF, self.int1ThsYL)
		#self.i2c.write8(self.__INT1_TSH_YL, param)
		pass

	def __setInt1ThsZH(self):
		"sets INT1_THS_ZH(36h)"
		if (self.debug):
			print "DBG INT1_THS_ZH(36h) 0x%02X (%d)" & (self.int1ThsZH & 0xFF, self.int1ThsZH)
		#self.i2c.write8(self.__INT1_TSH_ZH, param)
		pass

	def __setInt1ThsZL(self):
		"sets INT1_THS_ZL(37h)"
		if (self.debug):
			print "DBG INT1_THS_ZL(37h) 0x%02X (%d)" & (self.int1ThsZL & 0xFF, self.int1ThsZL)
		#self.i2c.write8(self.__INT1_TSH_ZL, param)
		pass

	def __setInt1Duration(self):
		"set INT1_DURATION(38h)"
		param = (self.int1Wait << 7) + self.int1Duration
		if (self.debug):
			print "DBG INT1_DURATION(38h) 0x%02X (%d)" & (param & 0xFF, param)
		#self.i2c.write8(self.__INT1_DURATION, param)
		pass

	def readTempData(self):
		"Read a raw temperature data"
		temp = self.i2c.readU8(self.__OUT_TEMP)
		if (self.debug):
			print "DBG: temp: 0x%04X (%d)" % (temp & 0xFFFF, temp)

		return temp
		pass

	def readTempCelsius(self):
		"Read and returns temperature in Celsius"
		return self.__twos_comp(self.readTempData(), 8)
		pass

class Obj3D :
	x = None
	y = None
	z = None

class L3GD20LibraryTests(unittest.TestCase):
	def setUp(self):
		self.gyro = L3GD20(0x6B, False)
		pass

	def tearDown(self):
		self.gyro = None
		pass

	def test_reading_raw(self):
		count = 0
		print ""
		while count < 3:
			data = self.gyro.readData()
			print " Gyro X: 0x%04X (%d) Y: 0x%04X (%d) Z: 0x%04X (%d)" % (data.x & 0xFFFF, data.x, data.y & 0xFFFF, data.y, data.z & 0xFFFF, data.z)
			count = count + 1
		pass

	def test_reading_dps(self):
		count = 0
		print ""
		while count < 3:
			data = self.gyro.readDps()
			print " Gyro X: %8.3f dps Y: %8.3f dps Z: %8.3f dps" % (data.x, data.y, data.z)
			count = count + 1
		pass

	def test_reading_temp_raw(self):
		count = 0
		print ""
		while count < 3:
			data = self.gyro.readTempData()
			print " Temp: 0x%04X (%d)" % (data & 0xFFFF, data)
			count = count + 1
		pass

	def test_reading_temp(self):
		count = 0
		print ""
		while count < 3:
			data = self.gyro.readTempCelsius()
			print " Temp: %8.3f C" % (data)
			count = count + 1
		pass


if __name__ == '__main__':
	#unittest.main()
	suite = unittest.TestLoader().loadTestsFromTestCase(L3GD20LibraryTests)
	unittest.TextTestRunner(verbosity=2).run(suite)